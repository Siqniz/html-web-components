const path = require('path');

//Plugins
const htmlWebpackPlugin = require('html-webpack-plugin');
const miniExtractCssPlugin = require('mini-css-extract-plugin');

module.exports = {
	entry: './index.js',
	output: {
		filename: 'index.js',
		path: path.join(__dirname, './dist'),
	},
	module: {
		rules: [
			{
				test: /\$.scss/i,
				use: [miniExtractCssPlugin.loader, 'css-loader', 'sass-loader'],
			},
		],
	},
	plugins: [
		new htmlWebpackPlugin({
			title: 'Web Components',
			template: './src/index.html',
		}),
		new miniExtractCssPlugin(),
	],
};
